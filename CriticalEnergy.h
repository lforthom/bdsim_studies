#ifndef CriticalEnergy_h
#define CriticalEnergy_h

#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_sf_bessel.h>

static constexpr double c_light = GSL_CONST_MKSA_SPEED_OF_LIGHT;
static constexpr double h = GSL_CONST_MKSA_PLANCKS_CONSTANT_H;
static constexpr double hbar = GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR;
static constexpr double epsilon0 = GSL_CONST_MKSA_VACUUM_PERMITTIVITY;
static constexpr double qe = GSL_CONST_MKSA_ELECTRON_VOLT;
static constexpr double me =
    GSL_CONST_MKSA_MASS_ELECTRON / qe /* kg -> eV/c^2 */ * c_light * c_light /* eV/c^2 -> eV */ * 1e-9 /* eV -> GeV */;
static constexpr double mp =
    GSL_CONST_MKSA_MASS_PROTON / qe /* kg -> eV/c^2 */ * c_light * c_light /* eV/c^2 -> eV */ * 1e-9 /* eV -> GeV */;
static constexpr double alpha_em = 1. / 137.035999138;

double gamma(double beam_energy, double mass) { return beam_energy / mass; }

double bessel_k_integrated_to_infty(double nu, double x, double h = 0.5, double epsilon = 1.e-15) {
  double r_term = std::exp(-x) / 2. * h;
  size_t r = 0;
  double k = r_term;
  while (r_term > epsilon) {
    ++r;
    r_term = std::exp(-x * std::cosh(r * h)) * std::cosh(nu * r * h) / std::cosh(r * h);
    k += r_term * h;
  }
  return k;
}

double dipole_bending_radius(double beam_energy, double b_field) {
  const auto beam_momentum = std::sqrt(beam_energy * beam_energy - me * me);  // in GeV
  return beam_momentum / std::fabs(b_field);
  /*const auto gm = gamma(beam_energy, me), beta = std::sqrt(1. - 1. / gm / gm);
  return 10. / 2.998 * beta * beam_energy / std::fabs(b_field);*/
}

/// critical energy in GeV for dipole and electron beam
double dipole_critical_energy(double beam_energy, double b_field) {
  /*const auto omega_c =
      1.5 * std::pow(gamma(beam_energy, me), 3) * c_light * c_light / dipole_bending_radius(beam_energy, b_field);
  return h * omega_c;*/
  return 0.665 * beam_energy * beam_energy * std::fabs(b_field) * 1.e-6;
}

double dipole_spectrum_vs_angle(double b_field, double beam_energy, double angle, double energy_ev) {
  const auto vovr =
      c_light * c_light * b_field * std::sqrt(1 - std::pow(me / (beam_energy * 1e9), 2)) / (beam_energy * 1e9);
  const auto gm = gamma(beam_energy, me), gm2 = gm * gm;
  const auto angle2 = angle * angle;
  const auto wovwc4 = energy_ev / (1.5 * gm * gm * gm * vovr * hbar / qe);
  const auto xi = 0.5 * wovwc4 * std::pow(1 + gm2 * angle2, 1.5);
  const auto k2 = std::pow(gsl_sf_bessel_Kn(2. / 3., xi), 2), k1 = std::pow(gsl_sf_bessel_Kn(1. / 3., xi), 2);
  const auto alpha = qe * qe / (4. * M_PI * M_PI * M_PI * epsilon0 * c_light * hbar);
  return 0.75e-6 * alpha * gm2 * 0.5e-3 / qe * std::pow(wovwc4 * (1 + gm2 * angle2), 2) *
         (k2 + gm2 * angle2 / (1 + gm2 * angle2) * k1);
}

double dipole_spectrum(double b_field, double beam_energy, double energy_gev) {
  const auto y = energy_gev / dipole_critical_energy(beam_energy, b_field);
  return 9. * std::sqrt(3.) / 8. * M_PI * y * bessel_k_integrated_to_infty(5. / 3., y);
}

#endif
