#ifndef BDSIMFileUtils_h
#define BDSIMFileUtils_h

#include <TFile.h>
#include <TH1.h>
#include <TTree.h>
#include <TTreeReader.h>

#include <vector>

double get_median(TH1* hist) {
  double quantiles[1] = {0.5}, median{0.};
  const auto n_quantiles = hist->GetQuantiles(1, &median, quantiles);
  if (n_quantiles != 1)
    throw runtime_error("failed to extract median");
  return median;
}

struct ElementInfo {
  string name;
  float spos, smin, smax;
  float bfield;
  string aper_type;
  array<float, 4> aper_params;
};

struct Beamline {
  static Beamline build_from_file(const string& filename) {
    Beamline beamline;
    unique_ptr<TFile> file(TFile::Open(filename.data()));
    auto* mdl = file->Get<TTree>("Model");
    TTreeReader model_reader(mdl);
    TTreeReaderValue<vector<string> > names(model_reader, "Model.componentName");
    TTreeReaderValue<vector<float> > lengths(model_reader, "Model.length");
    TTreeReaderValue<vector<float> > smins(model_reader, "Model.staS");
    TTreeReaderValue<vector<float> > scoords(model_reader, "Model.midS");
    TTreeReaderValue<vector<float> > smaxs(model_reader, "Model.endS");
    TTreeReaderValue<vector<string> > aperTypes(model_reader, "Model.beamPipeType");
    TTreeReaderValue<vector<double> > aper1s(model_reader, "Model.beamPipeAper1");
    TTreeReaderValue<vector<double> > aper2s(model_reader, "Model.beamPipeAper2");
    TTreeReaderValue<vector<double> > aper3s(model_reader, "Model.beamPipeAper3");
    TTreeReaderValue<vector<double> > aper4s(model_reader, "Model.beamPipeAper4");
    TTreeReaderValue<vector<float> > bfields(model_reader, "Model.bField");
    model_reader.Next();
    for (size_t j = 0; j < names->size(); ++j) {
      string elem_name = names->at(j);
      if (auto pos = elem_name.find("_even_ang"); pos != string::npos)
        elem_name = elem_name.erase(pos, elem_name.length());
      if (elem_name.empty())
        continue;
      const float spos = scoords->at(j);
      beamline.elements.emplace_back(
          ElementInfo{elem_name,
                      spos,
                      smins->at(j),
                      smaxs->at(j),
                      bfields->at(j),
                      aperTypes->at(j),
                      {(float)aper1s->at(j), (float)aper2s->at(j), (float)aper3s->at(j), (float)aper4s->at(j)}});
      //cout << elem_name << "(" << spos << ") -> " << aperTypes->at(j) << endl;
    }
    return beamline;
  }
  vector<ElementInfo> elements;
};

#endif
