#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:05:20 2023

@author: lforthom
"""

import argparse
import logging

class Parser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__()
        self.add_argument('nruns', type=int, default=10)
        self.add_argument('ngen', type=int, default=1000)
        self.add_argument('--output', type=str, default='')
        self.add_argument('--seed', type=int, default=42)
        self.add_argument('--jobid', type=int, default=-1)
        self.add_argument('--quiet', action=argparse.BooleanOptionalAction)
        self.add_argument('--keep-bdsim-output', action=argparse.BooleanOptionalAction)
        self.add_argument('--keep-rebdsim-output', action=argparse.BooleanOptionalAction)
        self.add_argument('--record-photons-only', action=argparse.BooleanOptionalAction)
        self.add_argument('-d', '--debug', help="Enable debugging mode",
                          action="store_const", dest="loglevel",
                          const=logging.DEBUG, default=logging.INFO)

    def parse(self):
        args = self.parse_args()
        logging.basicConfig(level=args.loglevel)
        return args
