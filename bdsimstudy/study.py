#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:05:20 2023

@author: lforthom
"""

import pybdsim
import os
import numpy as np
import ROOT

class Study(object):
    def __init__(self, args, runname: str):
        self.machine = pybdsim.Builder.Machine()
        self.beam = pybdsim.Beam.Beam()  # beam definition
        self.options = pybdsim.Options.Options()  # simulation options

        self._seed = args.seed
        self._quiet_mode = args.quiet
        self._ngenerate = args.ngen  # (num primary particles i.e. eletrons)
        self._record_photons_only = args.record_photons_only
        self._run_name = runname
        if args.jobid >= 0:
            self._run_name += f'_{args.jobid}'
        self._buffers = dict()
        self._gmad_files_generated = False

        self._gmad_path = f'GMAD_{self._run_name}'
        if not os.path.isdir(self._gmad_path):
            os.makedirs(self._gmad_path, exist_ok=True)
        self._data_path = f'DATA/{self._run_name}'
        if not os.path.isdir(self._data_path):
            os.makedirs(self._data_path, exist_ok=True)
        self._bdsim_keep_output_file = args.keep_bdsim_output
        self._bdsim_output_filename = ''
        self._rebdsim_input_commands = []
        self._rebdsim_output_filename = ''
        self._rebdsim_dirname = 'Event'
        self._rebdsim_keep_output_file = args.keep_rebdsim_output
        if self._quiet_mode:
            self.setOption('options', 'verbose', False)
            self.setOption('options', 'verboseRunLevel', False)
            self.setOption('options', 'verboseEventLevel', False)
            self.setOption('options', 'verboseTrackingLevel', False)
            self.setOption('options', 'verboseSteppingLevel', False)

    def setOption(self, field: str, key: str, value):
        val = value
        if type(value) == str:
            val = '"' + val + '"'  # use double here so when written to gmad file it outputs with "..."
        elif type(value) == bool:
            val = 1 if val else 0
        if field == 'options':
            self.options[key] = val
        elif field == 'beam':
            self.beam[key] = val
        else:
            raise RuntimeError(f'Key type "{type}" was not defined.')

    def fileInPath(self, type: str, filename: str):
        if type == 'gmad':
            return os.path.join(self._gmad_path, filename)
        elif type == 'data':
            return os.path.join(self._data_path, filename)
        raise RuntimeError(f'Key type "{type}" was not defined.')

    def bookRebdsimVariable(self, varname: str, nbins: int, rng: tuple[float, float], var: str, cut: str=''):
        """
        Generate a rebdsim file containing all the histograms which are required to produce the desired output data.

        See BDSIM docs for rebdsim examples and explanations.
        """
        self._rebdsim_input_commands.append(f'SimpleHistogram1D {self._rebdsim_dirname}. {varname} {{{{{nbins}}}}} {{{{{rng[0]}:{rng[1]}}}}} {var} {cut}')

    ### buffer accessor methods

    def book(self, bufname: str):
        self._buffers[bufname] = []

    def append(self, bufname: str, value):
        if bufname not in self._buffers.keys():
            raise RuntimeError(f'Key "{bufname}" not found in list of booked buffers.')
        self._buffers[bufname].append(value)

    def rawBuffers(self):
        buffers = dict()
        for key, value in self._buffers.items():
            buffers[key] = np.array(value)
        return buffers

    def buffers(self):
        """
        For each buffer, provide the average over a set of nruns each with a different seed.
        Also returned is the standard error on the value + the range as an array with two values
        """
        from numpy.core.fromnumeric import std
        buffers = dict()
        for key, values in self.rawBuffers().items():
            # calculate the mean and the standard error
            value = np.mean(values)
            err = np.std(values) / np.sqrt(len(values))
            val_range = np.asarray([min(values), max(values)] if len(values) > 0 else [0., 0.])
            buffers[key] = (value, err, val_range)
        return buffers

    ### running utilities

    def study(self, *args, **kwargs):
        raise RuntimeError('Calling undefined study() method on an analysis object')

    def run(self, nruns: int=1, *args, **kwargs):
        """
        Runs the set study, must call genGMAD() before running (unless provided files manually).
        If providing manually the main gmad must be in the directory as 'GMAD/input.gmad'.
        By default the before and after samplers are named DRIFT_0 and self._colNames respectively.

        TODO: Needs updating to study extra material
        """
        self._generateGmadFiles()
        self._generateRebdsimInputFile()
        self._bdsim_run = False  # reset before running
        self._iter = -1
        for key, val in self._buffers.items():
            val = []
        for i in range(nruns):
            self._iter = i
            self.study(*args, **kwargs)
            if not self._bdsim_keep_output_file and os.path.isfile(self._bdsim_output_filename):
                os.remove(self._bdsim_output_filename)
                print('deleted', self._bdsim_output_filename)
            if not self._rebdsim_keep_output_file and os.path.isfile(self._rebdsim_output_filename):
                os.remove(self._rebdsim_output_filename)
                print('deleted', self._rebdsim_output_filename)

    def runBdsim(self, load: bool=True):
        # Important to make sure this directory exists where python being called from
        self._bdsim_output_filename = self.fileInPath('data', f'bdsim_{self._iter}')
        seed = self._seed + (self._iter * 42) + 23
        # run bdsim
        bdsim_options = [f'--seed={seed}']
        pybdsim.Run.Bdsim(self.fileInPath('gmad', 'input.gmad'),
                          self._bdsim_output_filename,
                          ngenerate=self._ngenerate,
                          options=bdsim_options)

        self._bdsim_run = True
        self._bdsim_output_filename += '.root'
        if not os.path.isfile(self._bdsim_output_filename):
            raise IOError(f'BDSIM output file "{self._bdsim_output_filename}" was not found in output directory.')
        if load:  # load the bdsim data from last run
            return pybdsim.Data.Load(self._bdsim_output_filename)
        return self._bdsim_output_filename

    def runRebdsim(self, load: bool=True, bdsim_file: str=''):
        if not bdsim_file and not self._bdsim_run:
            bdsim_file = self.runBdsim(load=False)
        if not bdsim_file:
            raise IOError(f'Invalid BDSIM output file: "{bdsim_file}".')

        # Important to make sure this directory exists where python being called from
        self._rebdsim_output_filename = self.fileInPath('data', f'rebdsim_{self._iter}') + '.root'

        # run rebdsim
        pybdsim.Run.Rebdsim(self.fileInPath('gmad', 'rebdsim-input.txt'), bdsim_file, self._rebdsim_output_filename)

        self._bdsim_run = False  # reset before next run

        if load:  # load the bdsim data from last run
            return pybdsim.Data.Load(self._rebdsim_output_filename)
        return self._rebdsim_output_filename

    ### output management

    def storePickle(self, outputfile: str=''):
        if not outputfile:
            outputfile = f'result_{self._run_name}.pkl'
        import pickle
        with open(self._generateFilename(outputfile, 'pkl'), 'wb') as pkl:
            pickle.dump(self, pkl, pickle.HIGHEST_PROTOCOL)

    def storeROOT(self, outputfile: str=''):
        if not outputfile:
            outputfile = f'result_{self._run_name}.root'
        file = ROOT.TFile(self._generateFilename(outputfile, 'root'), 'recreate')
        tree = ROOT.TTree('run', 'quantiles obtained for each run')
        buffers = self.rawBuffers()
        tmp = dict()
        num_runs = 0
        for key, values in buffers.items():  # one pass to define branches
            tmp[key] = np.array([0], dtype=np.float64)
            tree.Branch(key, tmp[key], f'{key}/D')  #FIXME support other branch types
            num_runs = len(values)
        for i in range(num_runs):  # another pass to fill the tree
            for key, values in buffers.items():
                tmp[key][0] = values[i]
            tree.Fill()
        file.Write()
        file.Close()

    ### internal utilities

    def _generateFilename(self, name: str, extension: str=''):
        def _getExtension(filename):
            tokens = filename.split('.')
            if len(tokens) > 1:
                return tokens[-1]
            return None
        ext = _getExtension(name)
        if ext and ext == extension:
            return name
        return '.'.join([name, extension])

    def _generateGmadFiles(self):
        """
        Generate a set of GMAD files to the particular specification of this study as defined
        by the passed parameters when intiating this instance.

        This function is important to change when studying a different IR.

        The shielding material parameters are calculated based on the aperture sizes.

        TODO:   This could be adapted to allow the user to pass there own machine/options/beam
                resulting in no need to adapt this function.
                this could be more streamlined and allow for much more studies
        """
        if self._gmad_files_generated:
            raise RuntimeError('GMAD files were already generated.')

        if self._record_photons_only:
            self.machine.AddSampler('all, partID={22}')
        else:
            self.machine.AddSampler('all')
        self.machine.AddBeam(self.beam)
        self.machine.AddOptions(self.options)
        # Path is relative to where run from so be careful these directories are created before
        # the start of running, for example I have used the os package to ensure each run is in same place
        # Write the gmad output to this location.
        self.machine.Write(self.fileInPath('gmad', 'input'))
        self._gmad_files_generated = True

    def _generateRebdsimInputFile(self):
        with open(self.fileInPath('gmad', 'rebdsim-input.txt'), 'w') as f:
            f.writelines('\n'.join(self._rebdsim_input_commands))
