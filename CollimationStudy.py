#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:05:20 2023

@author: lforthom
"""

from bdsimstudy import study, parser
from datetime import datetime
import threading
import argparse
import math

class CollimationStudy(study.Study):
    def __init__(self, args, runkey):
        super().__init__(args, runkey)
        self._colMat = args.material  # (the material of the shielding being studied)
        self._colNames = ["COL_END", "COL_BEND"]  # (names of collimators)
        self._thickness = args.thickness  # in metres
        self._lstar = args.lstar
        self._lstar_scaling = self._lstar / 7.85153*2  # linear scaling parameter
        self._angle_lstar = 0.007473978*2 / self._lstar_scaling
        print(self._angle_lstar, self._lstar_scaling)

        self.eAperture = 0.00015  # metre (half x-y size)
        self.pAperture = 0.02  # metre (half x-y size)
        self.sep = 0.106105  # metre (seperation of beam centroid)
        self._extraT = args.extra_shielding
        self._modelBeampipe = args.model_beampipe
        self._pAperture_upstream = args.aperture_size  # metre

        # self.machine.AddIncludePre("material_Concretes.gmad")
        self.machine.AddIncludePre('extra.gmad')

        # Start definition of lattice
        if self._lstar > 20.:
            self.machine.AddDrift('uDRIFT50', 6.739)
            q0f_gradient = 29.0115938     # T/m
            q0f_length = 1.85484          # m
            q0f_angle = 0.00083467        # rad
            q0d_gradient = -21.87474917   # T/m
            q0d_length = 1.85484          # m
            q0d_angle = 0.00083467        # rad
            bend_length = 11.951172086*2  # m
            bend_angle = 5.378e-3*2       # m
        else:
            self.machine.AddDrift('uDRIFT50', 0.5)
            self.machine.AddDrift('uDRIFT_Q0', 1.871978)
            self.machine.AddDrift('uDRIFT30_0', 0.3)
            q0f_gradient = 30.13370   # T/m
            q0f_length = 2.1771258    # m
            q0f_angle = 0.002072436   # rad
            q0d_gradient = -18.36839  # T/m
            q0d_length = 2.1771258    # m
            q0d_angle = 0.002072436   # rad
            bend_length = 7.85153*2   # m
            bend_angle = 5.378e-3*2   # m

        self.machine.AddDipole('uBEND_QY', length=q0d_length, angle=q0d_angle, k1=q0d_gradient / 166.7778)
        self.machine.AddDrift('uDRIFT20', 0.2)
        self.machine.AddDipole('uBEND_QX', length=q0f_length, angle=q0f_angle, k1=q0f_gradient / 166.7778)
        self.machine.AddDrift('uDRIFT30_1', 0.3 - self._extraT)
        if self._extraT > 0.:
            # extra shielding to reduce synchrotron fan
            sep = 0.029
            eAp = 0.0025
            # width of first collimater so that proton aperture is in correct place
            width = 2. * (sep - eAp)
            self.machine.AddRCol(f'{self._colNames[1]}_0',
                                 self._extraT,
                                 material=self._colMat,
                                 xsize=self._pAperture_upstream/2,
                                 ysize=self._pAperture_upstream/2,
                                 horizontalWidth=width,
                                 offsetX=((width/2)+eAp))        # long dipole centre is IP
        if self._modelBeampipe:
            self.machine.AddDipole('BEND_0_pre', length=3.98288*2, angle=0.003791357543897814)
            self.machine.AddElement('BEND_0',
                                    length=7.7373,
                                    angle=0.007365240912204373,
                                    outerDiameter=1,
                                    geometryFile="gdml:LHeC_circ-ell-beam-pipe_2.5-10cm_sym.gdml",
                                    fieldAll="detectorfield",
                                    horizontalWidth=0.5)
            self.machine.AddDipole('BEND_0_post',
                                   length=3.98288*2,
                                   angle=0.003791357543897814)
        else:
            self.machine.AddDipole('BEND_0', length=bend_length, angle=bend_angle)
        self.machine.AddDrift('dDRIFT30_0', 0.3)
        self.machine.AddDipole('dBEND_QX', length=q0f_length, angle=q0f_angle, k1=q0f_gradient / 166.7778)
        self.machine.AddDrift('dDRIFT20', 0.2)
        self.machine.AddDipole('dBEND_QY', length=q0d_length, angle=q0d_angle, k1=q0d_gradient / 166.7778)
        if self._lstar > 20.:
            self.machine.AddDrift('dDRIFT50', 6.739 - self._thickness)
        else:
            self.machine.AddDrift('dDRIFT30_1', 0.3)
            self.machine.AddDrift('dDRIFT_Q0', 1.871978)
            self.machine.AddDrift('dDRIFT50', 0.5 - self._thickness)

        # Add two gmad files which contain extra information. The first contains different
        # definitions of concrete (recommend to comment out this line if not required).
        # The second file is essential for the collimation to work. The extra.gmad file contains
        # the definition of extra externally placed collimater which ensures the whole synchrotron
        # fan is incident on the material.
        ##
        # Definitions and placements of the shielding material being studied.
        # Do not reccomend changing this unless required, the material placement is determined
        # based in the definitions given at the start.
        width = 2. * (self.sep - self.eAperture)  # width of first collimater so that proton aperture is in correct place
        self.machine.AddRCol(f'{self._colNames[0]}_0',
                             self._thickness,
                             material=self._colMat,
                             xsize=self.pAperture,
                             ysize=self.pAperture,
                             horizontalWidth=width,
                             offsetX=((width/2)+self.eAperture))
        self.machine.AddPlacement(f'{self._colNames[0]}_1_p',
                                  bdsimElement=f'"{self._colNames[0]}_1"',
                                  referenceElement=f'"{self._colNames[0]}_0"',
                                  x=(width / 2. + width + self.eAperture))
        self.machine.AddPlacement(f'{self._colNames[0]}_2_p',
                                  bdsimElement=f'"{self._colNames[0]}_2"',
                                  referenceElement=f'"{self._colNames[0]}_0"',
                                  x=(width / 2. + width * 2. +self.eAperture))
        # open the extra.gmad file and replace it with new definition with identical thickness and material
        with open(self.fileInPath('gmad', 'extra.gmad'), 'w') as f:
            f.write(('{}_1: rcol, horizontalWidth={}, l={}, material="{}", xsize=0.0, ysize=0.0;\n'
                   +' {}_2: rcol, horizontalWidth={}, l={}, material="{}", xsize=0.0, ysize=0.0;\n'
                   +' {}_1: rcol, horizontalWidth={}, l={}, material="{}", xsize=0.0, ysize=0.0;').format(
                self._colNames[0], width, self._thickness, self._colMat,
                self._colNames[0], width, self._thickness, self._colMat,
                self._colNames[1], (0.029-0.005)*2, self._extraT, self._colMat))
            if self._modelBeampipe:
                f.write('\n detectorfield: field, type="dipole", fieldParameters="field=-0.1611019 by=1.0";')
        # copy the beamline GDML to the local directory
        if self._modelBeampipe:
            import os
            os.symlink(os.path.abspath('LHeC_circ-ell-beam-pipe_2.5-10cm_sym.gdml'), self.fileInPath('gmad', 'LHeC_circ-ell-beam-pipe_2.5-10cm_sym.gdml'))
        # copy the custom Geant4 commands
        if True:
            import os
            if not os.path.islink(self.fileInPath('gmad', 'emextraphysics.mac')):
                os.symlink(os.path.abspath('emextraphysics.mac'), self.fileInPath('gmad', 'emextraphysics.mac'))
        ##
        import time
        time.sleep(1)

        self.setOption('beam', 'X0', 0.)
        self.setOption('beam', 'Xp0', 0.)
        self.setOption('beam', 'Y0', 0.)
        self.setOption('beam', 'Yp0', 0.)
        if self._lstar > 20.:
          self.setOption('beam', 'alfx', -0.078885)
          self.setOption('beam', 'alfy', 6.279404)
          self.setOption('beam', 'betx', 0.434937)
          self.setOption('beam', 'bety', 1699.516133)
          self.setOption('beam', 'dispx', 0.051698)
          self.setOption('beam', 'dispxp', 0.003043)
          self.setOption('beam', 'dispy', 0.)
          self.setOption('beam', 'dispyp', 0.)
        else:
          self.setOption('beam', 'alfx', -0.035622)
          self.setOption('beam', 'alfy', 99.948526)
          self.setOption('beam', 'betx', 0.090510)
          self.setOption('beam', 'bety', 4881.193917)
          self.setOption('beam', 'dispx', 0.1337)
          self.setOption('beam', 'dispxp', 0.0121997)
          self.setOption('beam', 'dispy', 0.)
          self.setOption('beam', 'dispyp', 0.)

        self.setOption('beam', 'distrType', 'gausstwiss')
        self.setOption('beam', 'emitx', 5e-10)
        self.setOption('beam', 'emity', 5e-10)
        self.setOption('beam', 'energy', 50.)
        self.setOption('beam', 'particle', 'e-')
        self.setOption('beam', 'sigmaE', 2.8e-4)

        self.setOption('options', 'magnetGeometryType', 'none')
        self.setOption('options', 'preprocessGDML', 0)
        self.setOption('options', 'physicsList', 'synch_rad em')
        #self.setOption('options', 'physicsList', 'synch_rad') #FIXME
        self.setOption('options', 'hStyle', 1)
        self.setOption('options', 'beampipeMaterial', 'Cu')
        self.setOption('options', 'apertureType', 'elliptical')
        self.setOption('options', 'aper1', 0.5)
        self.setOption('options', 'aper2', 0.3)
        self.setOption('options', 'horizontalWidth', 1.05)
        self.setOption('options', 'worldMaterial', 'vacuum')
        self.setOption('options', 'maximumStepLength', 0.1)
        self.setOption('options', 'integratorSet', 'geant4')
        # allow for low-energy photons
        self.setOption('options', 'minimumKineticEnergy', 1.e-3)  # in GeV
        self.setOption('options', 'particlesToExcludeFromCuts', '22')
        self.setOption('options', 'geant4PhysicsMacroFileName', 'emextraphysics.mac')

        # book all the reBDSIM variables used in the analysis
        self.bookRebdsimVariable('NPhotons_dDRIFT50_cuts_1', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22&dDRIFT50.zp>=0&dDRIFT50.x>={}&dDRIFT50.x<={}'.format(self.eAperture, (self.sep-self.pAperture)))
        self.bookRebdsimVariable('NPhotons_dDRIFT50_cuts_2', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22&dDRIFT50.zp>=0&dDRIFT50.x>={}'.format(self.sep+self.pAperture))
        self.bookRebdsimVariable('NPhotons_COL_END_0_cuts_1', 100, (0., 0.8),
                                 'COL_END_0.x', 'COL_END_0.partID==22&COL_END_0.zp>=0&COL_END_0.x>={}&COL_END_0.x<={}'.format(self.eAperture, (self.sep-self.pAperture)))
        self.bookRebdsimVariable('NPhotons_COL_END_0_cuts_2', 100, (0., 0.8),
                                 'COL_END_0.x', 'COL_END_0.partID==22&COL_END_0.zp>=0&COL_END_0.x>={}'.format(self.sep+self.pAperture))
        self.bookRebdsimVariable('NPhotons_eAper', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22&dDRIFT50.zp>=0&dDRIFT50.x<={}&dDRIFT50.x>={}'.format(self.eAperture, -self.eAperture))
        self.bookRebdsimVariable('NPhotons_pAper', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22&dDRIFT50.zp>=0&dDRIFT50.x>={}&dDRIFT50.x>={}'.format(self.sep-self.pAperture, (self.sep+self.pAperture)))
        self.bookRebdsimVariable('NPhotons_dDRIFT50_total', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22')
        self.bookRebdsimVariable('NPhotons_dDRIFT50_zp', 100, (0., 0.8),
                                 'dDRIFT50.x', 'dDRIFT50.partID==22&dDRIFT50.zp>=0')
        self.bookRebdsimVariable('Photons_dDRIFT50_energy', 500, (0., 1.e-3),
                                 'dDRIFT50.energy', 'dDRIFT50.partID==22')
        self.bookRebdsimVariable('Photons_dDRIFT50_total_energy', 500, (0., 0.01),
                                 'Sum$(dDRIFT50.energy*(dDRIFT50.partID==22))', '1')

        self.book('absorption')  # buffer to store the percentage absorbed on each run
        # Storage for particular data
        self.book('totalPhotons')
        self.book('eAperPhotons')
        self.book('pAperPhotons')
        self.book('zpPhotons')
        self.book('cePhotons')
        self.book('eneMeanPhotons')

    def study(self):
        from array import array
        d = self.runRebdsim(load=True)

        self.append('totalPhotons', d.histogramspy['Event/SimpleHistograms/NPhotons_dDRIFT50_total'].entries)
        self.append('eAperPhotons', d.histogramspy['Event/SimpleHistograms/NPhotons_eAper'].entries)
        self.append('pAperPhotons', d.histogramspy['Event/SimpleHistograms/NPhotons_pAper'].entries)
        self.append('zpPhotons', d.histogramspy['Event/SimpleHistograms/NPhotons_dDRIFT50_zp'].entries)
        # compute the critical energy at dDRIFT50
        ce = array('d', [0.])
        d.histogramspy['Event/SimpleHistograms/Photons_dDRIFT50_total_energy'].hist.GetQuantiles(1, ce, array('d', [0.5]))
        print(ce)
        self.append('cePhotons', ce[0])
        #self.append('eneMeanPhotons', d.histogramspy['Event/SimpleHistograms/Photons_dDRIFT50_total_energy'].entries)

        numBefore = (d.histogramspy['Event/SimpleHistograms/NPhotons_dDRIFT50_cuts_1'].entries +
                     d.histogramspy['Event/SimpleHistograms/NPhotons_dDRIFT50_cuts_2'].entries)
        numAfter = (d.histogramspy['Event/SimpleHistograms/NPhotons_COL_END_0_cuts_1'].entries +
                    d.histogramspy['Event/SimpleHistograms/NPhotons_COL_END_0_cuts_2'].entries)

        # append the percentage absorbed to buffer
        self.append('absorption', 1. - numAfter * 1. / numBefore if numBefore > 0 else 0.)


def study(args, nruns, output=''):
    date = datetime.now().strftime('%Y%m%d%H%M')
    #runKey = f'lhec_{colMat}_th{thickness}_{date}'
    runKey = f'lhec_{args.material}_th{args.thickness}_lstar{int(args.lstar)}'
    if args.extra_shielding > 0.:
        runKey += f'_shield{args.extra_shielding}'
    if args.aperture_size != 0.01035:
        runKey += f'_aperSize{args.aperture_size}'
    s = CollimationStudy(args, runKey)
    s.run(nruns=nruns)
    print(s.buffers())
    s.storeROOT(output)


if __name__ == '__main__':
    ps = parser.Parser()
    ps.add_argument('thickness', type=float, default=0.04, nargs='?')
    ps.add_argument('material', type=str, default='Pb', nargs='?')
    ps.add_argument('--lstar', type=float, default=7.85153*2)
    ps.add_argument('--extra-shielding', type=float, default=0.)
    ps.add_argument('--num-threads', '-t', type=int, default=1)
    ps.add_argument('--model-beampipe', action=argparse.BooleanOptionalAction)
    ps.add_argument('--aperture-size', type=float, default=0.01035)
    args = ps.parse()

    if args.num_threads == 1:
        study(args, args.nruns, args.output)
    else:
        thread_list = []
        for tid in range(args.num_threads):
            thread = threading.Thread(target=study,
                                      args=(args, math.ceil(args.nruns / args.num_threads), f'{args.output}_{tid}.root'))
            thread_list.append(thread)
            thread.start()
