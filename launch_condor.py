#!/usr/bin/env python3

import htcondor
import argparse
import os
import logging

parser = argparse.ArgumentParser(description='A submission script for HTCondor')
parser.add_argument('job_name', type=str)
parser.add_argument('-c', '--script', type=argparse.FileType('r'))
parser.add_argument('--args', nargs='+', default=[])
parser.add_argument('-n', '--njobs', type=int, default=1)
parser.add_argument('-f', '--flavour', type=str, default='longlunch', choices=['espresso', 'microcentury', 'longlunch', 'workday', 'tomorrow', 'testmatch', 'nextweek'])
parser.add_argument('-i', '--inputs', nargs='+', default=[])
parser.add_argument('--send-credentials', action='store_true', default=True)
parser.add_argument('--num-cpus', type=int, default=1)
parser.add_argument('-w', '--work-dir', type=str, default=os.getcwd())
parser.add_argument('-d', '--debug', help="Enable debugging mode", action="store_const", dest="loglevel", const=logging.DEBUG, default=logging.INFO)
args = parser.parse_args()

run_script = 'submit_jobs.sh'
#bdsim_env = '/cvmfs/beam-physics.cern.ch/bdsim/x86_64-centos7-gcc11-opt/bdsim-env-v1.7.4-g4v10.7.2.3-ftfp-boost.sh'
#bdsim_env = '/cvmfs/beam-physics.cern.ch/bdsim/x86_64-centos7-gcc11-opt/bdsim-env-v1.7.6-g4v10.7.2.3-ftfp-boost.sh'
bdsim_env = '/cvmfs/beam-physics.cern.ch/bdsim/x86_64-el9-gcc13-opt/bdsim-env-v1.7.7-g4v10.7.2.3-ftfp-boost.sh'
logging.basicConfig(level=args.loglevel)
if not os.path.isdir(args.work_dir):
    logging.warning(f'Working directory {args.work_dir} does not exist. It will be created.')
    os.mkdir(args.work_dir)
#if os.path.isfile(run_script):
#    raise RuntimeError('Job submission script already exists!')

with open(run_script, 'w') as f:
    SUBMISSION_TEMPLATE=f"""#!/bin/sh
echo "Sourcing environment"
alias python=python3
source {bdsim_env}
echo "Starting job w/ command '${{@:1}}'"
${{@:1}}"""
    f.write(SUBMISSION_TEMPLATE)

if os.path.isfile(run_script):
    job_name = f'{args.job_name}.$(ClusterId).$(ProcId)'
    output_file = f'{job_name}.root'
    logging.info(f'output: {output_file}')
    base_commands = [
        '--output', output_file,
        '--seed', '$(seed_value)',
        '--jobid', '$(ProcId)',
        #'--keep-rebdsim-output',
    ]
    full_command = ' '.join(['python', args.script.name] + args.args + base_commands)
    logging.info(f'full command: {full_command}')

    col = htcondor.Collector()
    credd = htcondor.Credd()
    credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

    sub = htcondor.Submit()
    sub['Executable'] = run_script
    sub['Arguments'] = full_command
    sub['Output'] = f'{args.work_dir}/{job_name}.out'
    sub['Error'] = f'{args.work_dir}/{job_name}.err'
    sub['Log'] = f'{args.work_dir}/{job_name}.log'
    sub['+Flavour'] = args.flavour
    sub['max_retries'] = 5
    sub['MY.SendCredentials'] = args.send_credentials
    sub['request_cpus'] = args.num_cpus
    sub['seed'] = '$(ProcId) + $(ClusterId)'
    sub['seed_value'] = '$INT(seed)'
    sub['stream_output'] = True
    sub['stream_error'] = True
    sub['should_transfer_files'] = 'YES'
    sub['transfer_input_files'] = ','.join([args.script.name, 'bdsimstudy'] + args.inputs)
    sub['transfer_output_files'] = ','.join([output_file, 'DATA'])
    sub['periodic_hold'] = '(JobStatus == 2) && (time() - EnteredCurrentStatus) > (2 * 3600)'
    sub['periodic_hold_reason'] = '"Job ran for more than two hours"'
    sub['periodic_hold_subcode'] = 42
    sub['periodic_release'] = '(HoldReasonSubCode == 42)'
    logging.debug(f'last submission:\n{sub}')

    res = htcondor.Schedd().submit(sub, count=args.njobs)
    logging.debug(res)

#os.remove(run_script)
